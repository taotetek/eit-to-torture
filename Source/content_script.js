walk(document.body);

function walk(node) 
{
	// I stole this function from here:
	// http://is.gd/mwZp7E
	
	var child, next;

	switch ( node.nodeType )  
	{
		case 1:  // Element
		case 9:  // Document
		case 11: // Document fragment
			child = node.firstChild;
			while ( child ) 
			{
				next = child.nextSibling;
				walk(child);
				child = next;
			}
			break;

		case 3: // Text node
			handleText(node);
			break;
	}
}

function handleText(textNode) 
{
	var v = textNode.nodeValue;

	v = v.replace(/\bEIT\b/g, "torture");
	v = v.replace(/\benhanced interrogation\b/g, "torture");
	v = v.replace(/\bEITs\b/g, "torturing some folks");
	v = v.replace(/\bEIT Enhanced Interrogation Techniques\b/g, "Torture");
	v = v.replace(/\bEnhanced Interrogation Techniques\b/g, "Torturing Some Folks");
	v = v.replace(/\bInterrogation Tactics\b/g, "Torturing Some Folks");
	v = v.replace(/\binterrogation tactics\b/g, "Torturing Some Folks");
	v = v.replace(/\binterrogation program\b/g, "torture program");
	v = v.replace(/\binterrogation techniques\b/g, "torture");
	v = v.replace(/\bforced rectal feeding\b/g, "rape with a foreign object");
	v = v.replace(/\bforced rectal feedings\b/g, "rapes with a foreign object");
	v = v.replace(/\brectal feeding\b/g, "rape with a foreign object");
	v = v.replace(/\brectal feedings\b/g, "rapes with a foreign object");
	textNode.nodeValue = v;
}


