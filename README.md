eit-to-torture
==============

Chrome extension that fixed Orwellian newspeak from articles about torture.  Changes:

* "EIT" to "torture"
* "enhanced interrogation" to "torture"
* "EITs to "torturing some folks"

Original source from panicsteve's ["cloud-to-butt"](http://github.com/panicsteve/cloud-to-butt/)  extension.

[Direct download of crx file](https://github.com/taotetek/eit-to-torture/blob/master/Torture.crx?raw=true)

Installation
------------

In Chrome, choose Window > Extensions.  Drag Torture.crx into the page that appears.
